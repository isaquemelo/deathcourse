from flask import Flask, render_template, request
from keras.models import load_model
import numpy as np
import random

import tensorflow as tf

from keras import backend as K


K.clear_session()
global model
model = load_model('data/trained-model.h5')

app = Flask(__name__)

@app.route('/')
@app.route('/index.html')
def index():
    return render_template('index.html')


@app.route('/send', methods=['POST', 'GET'])
def send():
    if request.method == 'POST':
        form = request.form

        keys = {'0': 'Ileso', '1': 'Morto'}

        form_sample = [form['week-day'], form['time'] + ':00', int(form['route']), form['accident-cause'],
                       form['accident-type'], "Com Vítimas Fatais", form['phase'], form['flow'], form['weather'],
                       form['type'], form['right-place'], form['environment'], "Morto", form['sex']]

        indexes = ['dia_semana', 'horario', 'br', 'causa_acidente', 'tipo_acidente',
                   'classificacao_acidente', 'fase_dia', 'sentido_via',
                   'condicao_metereologica', 'tipo_pista', 'tracado_via', 'uso_solo',
                   'estado_fisico', 'sexo']

        hot_array_dic = {'causa_acidente': {'Animais na Pista': [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                                            'Defeito mecânico em veículo': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                                            'Defeito na via': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                                            'Desobediência à sinalização': [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                                            'Dormindo': [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                                            'Falta de atenção': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                            'Ingestão de álcool': [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                                            'Não guardar distância de segurança': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                                            'Ultrapassagem indevida': [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                                            'Velocidade incompatível': [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]},
                         'classificacao_acidente': {'Com Vítimas Fatais': [0, 0, 1],
                                                    'Com Vítimas Feridas': [0, 1, 0],
                                                    'Sem Vítimas': [1, 0, 0]},
                         'condicao_metereologica': {'Ceu Claro': [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                                    'Chuva': [0, 0, 0, 1, 0, 0, 0, 0, 0],
                                                    'Granizo': [0, 0, 0, 0, 0, 0, 0, 1, 0],
                                                    'Ignorada': [0, 0, 0, 0, 1, 0, 0, 0, 0],
                                                    'Neve': [0, 0, 0, 0, 0, 0, 0, 0, 1],
                                                    'Nevoeiro/neblina': [0, 0, 0, 0, 0, 1, 0, 0, 0],
                                                    'Nublado': [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                                    'Sol': [0, 0, 1, 0, 0, 0, 0, 0, 0],
                                                    'Vento': [0, 0, 0, 0, 0, 0, 1, 0, 0]},
                         'dia_semana': {'Domingo': [1, 0, 0, 0, 0, 0, 0],
                                        'Quarta': [0, 0, 0, 0, 0, 1, 0],
                                        'Quinta': [0, 0, 0, 1, 0, 0, 0],
                                        'Segunda': [0, 0, 0, 0, 1, 0, 0],
                                        'Sexta': [0, 1, 0, 0, 0, 0, 0],
                                        'Sábado': [0, 0, 1, 0, 0, 0, 0],
                                        'Terça': [0, 0, 0, 0, 0, 0, 1]},
                         'estado_fisico': {'Ferido Grave': [0, 0, 1, 0],
                                           'Ferido Leve': [0, 1, 0, 0],
                                           'Ileso': [1, 0, 0, 0],
                                           'Morto': [0, 0, 0, 1]},
                         'fase_dia': {'Amanhecer': [0, 0, 0, 1],
                                      'Anoitecer': [0, 0, 1, 0],
                                      'Plena noite': [0, 1, 0, 0],
                                      'Pleno dia': [1, 0, 0, 0]},
                         'sentido_via': {'Crescente': [1, 0], 'Decrescente': [0, 1]},
                         'sexo': {'Feminino': [0, 1], 'Masculino': [1, 0]},
                         'tipo_acidente': {'Atropelamento de animal': [0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       1,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0,
                                                                       0],
                                           'Atropelamento de pessoa': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                                           'Capotamento': [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                           'Colisão Transversal': [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                           'Colisão com bicicleta': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                                           'Colisão com objeto fixo': [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                           'Colisão com objeto móvel': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                                           'Colisão frontal': [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                           'Colisão lateral': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                           'Colisão traseira': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                           'Danos Eventuais': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                                           'Derramamento de Carga': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                                           'Incêndio': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                                           'Queda de motocicleta / bicicleta / veículo': [0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          1,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          0],
                                           'Saída de Pista': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                           'Tombamento': [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]},
                         'tipo_pista': {'Dupla': [0, 1, 0],
                                        'Múltipla': [0, 0, 1],
                                        'Simples': [1, 0, 0]},
                         'tracado_via': {'Cruzamento': [0, 0, 1],
                                         'Curva': [0, 1, 0],
                                         'Reta': [1, 0, 0]},
                         'uso_solo': {'Rural': [0, 1], 'Urbano': [1, 0]}}

        def gen_single_hot3(sample):
            x = []

            for i in range(14):
                if i != 12:
                    if indexes[i] not in ['horario', 'br', 'idade', 'ano_fabricacao_veiculo']:
                        try:
                            if indexes[i] != 'classificacao_acidente':
                                x += list(hot_array_dic[indexes[i]][sample[i]])
                        except:
                            print("sample[i] == ", sample[i])
                            print("index[i] == ", indexes[i])

                    else:
                        if type(sample[i]) == str:
                            try:
                                x.append(int(sample[i][0:2]))
                            except:
                                print(indexes[i])
                        else:

                            x.append(sample[i])
            return x

        sample = []
        try:
            valor = form['random_button']
        except KeyError:
            valor = "Any"
        if valor == 'Randomize':
            for i in indexes:
                if i not in ['horario', 'br', 'idade', 'ano_fabricacao_veiculo']:
                    sample.append(random.choice(list(hot_array_dic[i].keys())))

                else:
                    if (i == 'horario'):
                        hours = random.randint(10, 24)
                        sample.append(str(hours) + ":00" + ":00")

                    if (i == 'br'):
                        route = random.choice(np.arange(105, 230, 10))
                        sample.append(route)

            x = []
            hoted = gen_single_hot3(sample)
            x.append(hoted)
            global model
            result = model.model.predict_proba(
                np.array(x), batch_size=1)
            status = keys[str(np.argmax(result))]
            text = ""
            if (status == "Morto"):
                text = "You got yourself killed!"
            elif status == "Ileso":
                text = "You've got some injuries."

            net_in = np.delete([sample], [5], axis=1)[0]

            indexes = ['dia_semana', 'horario', 'br', 'causa_acidente', 'tipo_acidente',
                       'fase_dia', 'sentido_via',
                       'condicao_metereologica', 'tipo_pista', 'tracado_via', 'uso_solo',
                       'sexo']

            net_in_str = []
            for i in range(len(indexes)):
                if indexes[i] == 'sexo':
                    net_in_str.append("{}: {}".format(indexes[i].replace('_', ' ').capitalize(), net_in[-1]))
                else:
                    net_in_str.append("{}: {}".format(indexes[i].replace('_', ' ').capitalize(), net_in[i]))


            return render_template('age.html', status=status, netoutput=result, choosen=net_in_str, random=1, text=text)

        x = []

        sample = gen_single_hot3(form_sample)
        x.append(sample)
        result = model.model.predict_proba(
            np.array(x), batch_size=1)

        status = keys[str(np.argmax(result[0]))]
        text = ""
        if (status == "Morto"):
            text = "You got yourself killed!"
        elif status == "Ileso":
            text = "You've got some injuries."

        net_in = np.delete([form_sample], [5], axis=1)[0]

        indexes = ['dia_semana', 'horario', 'br', 'causa_acidente', 'tipo_acidente',
                   'fase_dia', 'sentido_via',
                   'condicao_metereologica', 'tipo_pista', 'tracado_via', 'uso_solo',
                   'sexo']

        net_in_str = []
        for i in range(len(indexes)):
            if indexes[i] == 'sexo':
                net_in_str.append("{}: {}".format(indexes[i].replace('_', ' ').capitalize(), net_in[-1]))
            else:
                net_in_str.append("{}: {}".format(indexes[i].replace('_', ' ').capitalize(), net_in[i]))


        return render_template('age.html', status=status, netoutput=result, choosen=net_in_str, random=0, text=text)

    else:
        return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=False, threaded=False)
